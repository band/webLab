This folder is a workbench for user-experience experiments.  

## 2024-05-02
 - starting work on learning how to fashion hamburger menus for
   responsive webpages and more  

## 2024-05-07
 - some notes from Wikipedia layout:  
 ```
   vector-column
     vector-column-start
	   vector-main-menu-container
 
 ```

 - sidebar "Contents" css:

```
  vector-sticky-pinned-container
    vector-toc-contents
	  vector-toc-list-item
	    vector-toc-link
		  vector-toc-text

```
